package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args){

        Scanner act1 = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year:");
        int year = act1.nextInt();

        if(year % 4 == 0 || year % 400 == 0) {
            System.out.println(year + " is a leap year.");
        } else {
            System.out.println(year + " is NOT a leap year.");
        }
    }
}


