package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args){

        ArrayList<Integer> primeNumbers = new ArrayList<>(Arrays.asList(2, 3, 5, 7, 11));
        System.out.println("The first prime number is: " + primeNumbers.get(0));
        System.out.println("The second prime number is: " + primeNumbers.get(1));
        System.out.println("The third prime number is: " + primeNumbers.get(2));
        System.out.println("The fourth prime number is: " + primeNumbers.get(3));
        System.out.println("The fifth prime number is: " + primeNumbers.get(4));

        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + myFriends);

        HashMap<String, Integer> inventory = new HashMap<>()
        {

            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
